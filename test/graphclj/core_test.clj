(ns graphclj.core-test
  (:require [clojure.test :refer :all]
            [graphclj.core :refer :all]
            [graphclj.tools :as tools]
            [graphclj.graph :as graph]
            [graphclj.centrality :as cent])
  (:use midje.sweet))


;; Test gen-graph
(deftest gen-graph-test
  (testing "Genere une hashmap contenant le graphe"
    (is (=
         (take 5 (graph/gen-graph (tools/readfile "enron_static.csv")))
        '([121 {:neigh #{65 130 31 113 56}}]
          [65 {:neigh #{121 70 74 128 59 20 48 31 113 56 89 12 66 82 83 67}}]
          [70 {:neigh #{121 65 7 20 1 55 149 54 129 56 118 89 122 29 28 134 12 66 115 9 83
     138 10 67}}]
          [62 {:neigh #{27 1 48 75 113 40 33 41 82 76 68 132 26 79 73 114 8}}]
          [74 {:neigh #{54 92 15 111 28 64}}]) ))))


;; Test ens-lab
(deftest ens-lab-test
  (testing "Genere un ensemble des centralites de chaque noeud, que l'on triera ensuite afin de pouvoir les classer ensuite (generer la cle `rank`)"
    (is (fact 
            (apply sorted-set
                   (let [g {0 {:neigh #{1 3}, :close 3.0}
                            1 {:neigh #{0 4 3}, :close 3.5}
                            2 {:neigh #{3}, :close 2.0833333333333335}
                            3 {:neigh #{0 1 2}, :close 3.5}
                            4 {:neigh #{1}, :close 2.0833333333333335}}]
                     (tools/ens-lab g :close))) => #{2.0833333333333335 3.0 3.5}))))


;; Test map-set
(deftest map-set-test
  (testing "Genere une map de l'ensemble"
    (is (=
         (let [s (apply sorted-set 
                        (let [g 
                              {0 {:neigh #{1 3}, :close 3.0}
                               1 {:neigh #{0 4 3}, :close 3.5}
                               2 {:neigh #{3}, :close 2.0833333333333335}
                               3 {:neigh #{0 1 2}, :close 3.5}
                               4 {:neigh #{1}, :close 2.0833333333333335}}]
                          (tools/ens-lab g :close)))]
           (tools/map-set s)) {2.0833333333333335 0, 3.0 1, 3.5 2}))))





;; Test rank-nodes
(deftest rank-nodes-test
  (testing "Retourne la map avec les bons rangs"
    (is (fact 
            (let [g {0 {:neigh #{1 3}, :close 3.0}
                     1 {:neigh #{0 4 3}, :close 3.5}
                     2 {:neigh #{3}, :close 2.0833333333333335}
                     3 {:neigh #{0 1 2}, :close 3.5}
                     4 {:neigh #{1}, :close 2.0833333333333335}}]
              (tools/rank-nodes g :close))
  => 

  {0 {:neigh #{1 3}, :close 3.0, :rank 1},
   1 {:neigh #{0 4 3}, :close 3.5, :rank 2},
   2 {:neigh #{3}, :close 2.0833333333333335, :rank 0},
   3 {:neigh #{0 1 2}, :close 3.5, :rank 2},
   4 {:neigh #{1}, :close 2.0833333333333335, :rank 0}}))))

;; Test distance
(deftest distance-test
  (testing "La distance entre chaque noeud du graph"
    (is (fact (let [g {0 {:neigh #{1 3}}
                       1 {:neigh #{0 4 3}}
                       2 {:neigh #{3}}
                       3 {:neigh #{0 1 2}}
                       4 {:neigh #{1}}}]
                (cent/distance g 1)) => {0 1.0, 4 1.0, 3 1.0, 1 0.0, 2 2.0}))))

;; Test closeness-all
(deftest closeness-all-test
  (testing "Returns the closeness for all nodes in graph g"
    (is (fact (let [g {0 {:neigh #{1 3}}
                       1 {:neigh #{0 4 3}}
                       2 {:neigh #{3}}
                       3 {:neigh #{0 1 2}}
                       4 {:neigh #{1}}}]
                (cent/closeness-all g)) 
          =>
              {0 {:neigh #{1 3}, :close 3.0},
               1 {:neigh #{0 4 3}, :close 3.5},
               2 {:neigh #{3}, :close 2.3333333333333335},
               3 {:neigh #{0 1 2}, :close 3.5},
               4 {:neigh #{1}, :close 2.333333333333333}}))))



;; Test erdos-renyi-rnd
(deftest erdos-renyi-rnd
  (testing "Genere un graph `erdos-renyi` aleatoire"
    (is (let [n 5] (= (count (graph/erdos-renyi-rnd n 0.1)) n)))))
