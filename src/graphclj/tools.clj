(ns graphclj.tools
  (:require [clojure.string :as str])
  (:use clojure.pprint))

(defn readfile [f]
    "Returns a sequence from a file f"
    (with-open [rdr (clojure.java.io/reader f)]
            (doall (line-seq rdr))))

(defn ens-lab [g l]
  (loop [g g
         ens #{}]
    (if (seq g)
      (recur (rest g) (conj ens (get (second (first g)) l)))
      ens)))

(defn map-set [s]
  (loop [s (apply sorted-set s)
         m {}
         i 0]
    (if (seq s)
      (recur (rest s) (assoc m (first s) i) (inc i))
      m)))


(defn rank-nodes [g,l]
  "Ranks the nodes of the graph in relation to label l in accending order"
  (loop [m (map-set (ens-lab g l))
         g g
         res g
         rank (get m  (get (second (first g)) l))]
    (if (seq g)
      (recur m (rest g) (assoc res (ffirst g) (assoc (second (first g)) :rank rank)) 
             (get m (get (second (first (rest g))) :close)))
      res)))


(defn generate-colors [n]
    (let [step 10]
    (loop [colors {}, current [255.0 160.0 122.0], c 0]
      (if (= c (inc n))
        colors
        (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
               (map #(mod (+ step %) 255) current) (inc c))
      ))))

(defn color-str [s]
  (loop [s s
         c "\""]
    (if (seq s)
      (if (seq (rest s))
        (recur (rest s) (str c (first s) " "))
        (recur (rest s) (str c (first s))))
      (str c "\""))))

(defn node-color [g]
  (loop [chaine "graph g{"
         g g
         color (generate-colors (count g))]
    (if (seq g)
       (recur (str chaine " " (ffirst g) " [style=filled color=" (color-str (get color (ffirst g)))  "]\n") (rest g) color)
      chaine)))

(defn node-connex [g]
  (loop [g g
         s ""
         m {}]
    (if (seq g)
      (let [conn  (loop [ens (get (second (first g)) :neigh)
                         ch ""]
                    (if (seq ens)
                      (if (contains? m (first ens))
                        (recur (rest ens) ch)
                        (recur (rest ens) (str ch " "(ffirst g) " -- " (first ens) "\n")))
                      ch))]
        (recur (rest g) (str s conn) (assoc m (ffirst g) (first (get (second (first g)) :neigh)))))
      s)))

(defn to-dot [g]
  "Returns a string in dot format for graph g, each node is colored in relation to its ranking"
  (let [chaine (node-color g)
        conn (node-connex g)]
    (str chaine conn "}")))

