(ns graphclj.graph
  (:require [clojure.string :as str]
            [graphclj.tools :as tls])
  (:use clojure.pprint))

;; Generate a graph from the lines
(defn gen-graph [lines]
    "Returns a hashmap contating the graph"
   (loop [mp {}
          lines lines]
     (if (seq lines)
       (let [ligne-seq (re-seq #"\d+" (first lines))
             int-noeud (Integer/parseInt (first ligne-seq))
             int-vois (Integer/parseInt (second ligne-seq))]
         (if (contains? mp int-noeud)
           (recur  (assoc mp int-noeud (assoc (get mp int-noeud) :neigh 
                                              (let [ens (get (get mp int-noeud) :neigh)]
                                                (conj ens int-vois)) ))
                   (rest lines))
           (recur  (assoc mp int-noeud (assoc {} :neigh (conj #{} int-vois) ))
                   (rest lines))))
       mp)))

(defn map-graph [n]
  "Build a graph with n nodes but without neighbors"
  (loop [g {}
         i 0]
    (if (< i n)
      (recur (assoc g i (assoc {} :neigh #{})) (inc i))
      g)))



(defn erdos-renyi-rnd [n,p]
  "Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"
  (loop [g (map-graph n)
         i 0
         j 0]
    (if (< i n)
      (if (not= i j)
        (if (< j n)
          (recur (let [r (rand 1)]
                   (if (<= r p)
                     (let [ens (get (get g j) :neigh)
                           ens2 (get (get g i) :neigh)
                           g (assoc g j (assoc (get g j) :neigh (conj ens i)))]
                       (assoc g i (assoc (get g i) :neigh (conj ens2 j))))
                     g)) (if (< j n)
                           i
                           (inc i)) (if (< j n)
                                      (inc j)
                                      0))
          (recur g (inc i) 0))
        (recur g (if (< j n)
                   i
                   (inc i))
               (if (< j n)
                 (inc j)
                 0)))
      g)))
