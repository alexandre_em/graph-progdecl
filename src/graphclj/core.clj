(ns graphclj.core
  (:require [graphclj.graph :as graph]
            [graphclj.tools :as tools]
            [graphclj.centrality :as central])
  (:use clojure.pprint)
  (:use clojure.java.io)
  (:gen-class))

(defn -main [& args]
  "Prend 2 arguments, le nom du fichier entree et le nom du fichier sortie(en .dot)"
  (if (not= 2 (count args))
    (throw (Throwable. (str "\nIl faut 2 arguments :\n `le nom du fichier entrant` ou `random`\n ET\n `le nom du fichier sortant en .dot`\n") ))
    (if (= (first args) "random")
      (do
        (println "Entrez le nombre de noeuds")
        (def node (read-line))
        (println "Entrez la probabilite de lien entre noeuds")
        (def prob (read-line))
        (let [g (graph/erdos-renyi-rnd (Integer/parseInt node) (Float/parseFloat prob))
              g (central/closeness-all g)
              g (tools/rank-nodes g :close)]
          (with-open [wrtr (writer (second args))]
            (.write wrtr (tools/to-dot g))))
        (println (str "fichier " (second args) " cree.")))
      (do
        (println "Creer un graph :\n   1. ... dont la centralite des noeuds\n   2. ... dont le degre des noeuds\ndu graph sont tries par ordre croissant\n")
        (def input read-line)
        (case (input)
          "1" (do
                (let [g (graph/gen-graph (tools/readfile (first args)))
                      g (central/closeness-all g)
                      g (tools/rank-nodes g :close)]
                  (with-open [wrtr (writer (second args))]
                    (.write wrtr (tools/to-dot g))))
                (println (str "fichier " (second args) " cree.")))
          "2" (do
                (let [g (graph/gen-graph (tools/readfile (first args)))
                      g (central/closeness-all g)
                      g (tools/rank-nodes g :close)]
                  (with-open [wrtr (writer (second args))]
                    (.write wrtr (tools/to-dot g))))
                (println (str "fichier " (second args) " cree."))))))))
